This notebook implements a solution for the 'Item Demand Forecasting Challange' using FBprophet and simple linear regression. With this approach we archieved a MAE of 6.7 on a six month validation set.

"This competition is provided as a way to explore different time series techniques on a relatively simple and clean dataset.

You are given 5 years of store-item sales data, and asked to predict 3 months of sales for 50 different items at 10 different stores.

What's the best way to deal with seasonality? Should stores be modeled separately, or can you pool them together? Does deep learning work better than ARIMA? Can either beat xgboost?

This is a great competition to explore different models and improve your skills in forecasting."

From: kaggle.com

The files can be found at: https://www.kaggle.com/c/demand-forecasting-kernels-only/overview